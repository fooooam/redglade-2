import 'package:flutter/material.dart';
import 'package:try_flutter/pages/home.dart';
import 'package:url_launcher/url_launcher.dart';

class AboutPage extends StatelessWidget {
  final String appUrl = "https://gitlab.com/fooooam/redglade-2";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[

          Padding(
            padding: const EdgeInsets.only(top: 40.0, left: 10, bottom: 10),
            child: Text(
              'Красная поляна',
              style: TextStyle(fontSize: 24, color: Colors.red)),
          ),

          Padding(
            padding: const EdgeInsets.all(0),
            child: Text(
              'Версия: 2.0.0', style: TextStyle(fontSize: 16, color: Colors.grey)
            ),
          ),

          Container(
            margin: const EdgeInsets.only(top: 15.0, bottom: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                    child: Text(
                        'Сайт проекта',
                        style: TextStyle(
                          color: Colors.red, fontSize: 18
                        )
                    ),
                    onPressed: () {
                      _openProjectSite();
                    }
                ),
              ],
            ),
          ),

          Container(
            margin: const EdgeInsets.only(bottom: 20),
            child: Text(
                'Новости проекта и новые версии',
                style: TextStyle(color: Colors.grey, fontSize: 14)
            ),
          ),

          Divider(),

          FlatButton(
            child: Text('Вернуться назад'),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => HomePage()),
              );
            },
          )
        ],
      ),
    );
  }

  _openProjectSite() async {
    if (await canLaunch(appUrl)) {
      await launch(appUrl);
    } else {
      throw 'Could not launch $appUrl';
    }
  }
}