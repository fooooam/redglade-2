import 'package:flutter/material.dart';
import 'package:try_flutter/models/trail.dart';
import 'package:try_flutter/pages/about.dart';
import 'package:try_flutter/resorts/gazprom.dart';
import 'package:try_flutter/resorts/gorkygorod.dart';
import 'package:try_flutter/resorts/rosakhutor.dart';

enum ResortType { RosaKhutor, Gazprom, GorkyGorod }

class HomePage extends StatefulWidget {
  @override
  HomePageState createState() => new HomePageState();
}

class HomePageState extends State<HomePage> {

  Container empty = new Container();

  bool filterTrailsOpened = false;

  bool filterTrailsBlack = false;
  bool filterTrailsRed = false;
  bool filterTrailsBlue = false;
  bool filterTrailsGreen = false;

  List<Trail> rosaKhutorTrails = [];
  List<Trail> gazpromTrails = [];
  List<Trail> gorkyGorodTrails = [];

  ResortType resortType = ResortType.RosaKhutor;

  TextStyle tabStyle = TextStyle(
      fontSize: 17.0
  );

  List<Trail> _getTrailsWithFilters(List<Trail> trails) {
    return trails.where(_isTrailAllowedByFilters).toList();
  }

  bool _isTrailAllowedByFilters(Trail trail) {
    bool result = true;

    if (this.filterTrailsOpened) {
      if (trail.status != TrailStatus.Open) {
        result = false;
      }
    }

    if (result && (filterTrailsBlue && (trail.difficulty != TrailDifficulty.Blue))) {
      result = false;
    }

    if (result && (filterTrailsRed && (trail.difficulty != TrailDifficulty.Red))) {
      result = false;
    }

    if (result && (filterTrailsBlack && (trail.difficulty != TrailDifficulty.Black))) {
      result = false;
    }

    if (result && (filterTrailsGreen && (trail.difficulty != TrailDifficulty.Green))) {
      result = false;
    }

    return result;
  }

  @override
  void initState() {
    super.initState();
    getGazpromTrailsData(context).then((items) => _appendItems(ResortType.Gazprom, items));
    getGorkyGorodTrailsData(context).then((items) => _appendItems(ResortType.GorkyGorod, items));
    getRosaKhutorTrailsData(context).then((items) => _appendItems(ResortType.RosaKhutor, items));
  }

  _appendItems(ResortType resortType, List<Trail> items) {
    setState(() {
      if (this.resortType == ResortType.RosaKhutor) {
        this.rosaKhutorTrails.clear();
        this.rosaKhutorTrails.addAll(items);

      } else if (this.resortType == ResortType.Gazprom) {
        this.gazpromTrails.clear();
        this.gazpromTrails.addAll(items);

      } else if (this.resortType == ResortType.GorkyGorod) {
        this.gorkyGorodTrails.clear();
        this.gorkyGorodTrails.addAll(items);
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 25),
      child: DefaultTabController(
        length: 3,
        child: Container(
          margin: EdgeInsets.only(top: 0),
          child: Scaffold(
            drawer: Drawer(
              child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  DrawerHeader(
                    child: DecoratedBox(decoration: BoxDecoration(
                      image: DecorationImage(image: AssetImage('images/logo.jpg'))
                    )),
                    padding: EdgeInsets.all(0),
                    margin: EdgeInsets.all(0),
                  ),

                  ListTile(
                      title: Text('Фильтры', style: TextStyle(fontSize: 24)),
                  ),

                  CheckboxListTile(
                      value: this.filterTrailsOpened,
                      title: Text('Только открытые трассы'),
                      activeColor: Colors.red,
                      onChanged: _toggleTrailsOpenedFilter
                  ),

                  Divider(color: Colors.grey, height: 1),

                  ListTile(
                      title: Text(
                        'Сложность трасс',
                        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 17)
                      )
                  ),
                  CheckboxListTile(
                      value: this.filterTrailsBlue,
                      title: Text(
                          'Синие', style: TextStyle(color: Colors.blue, fontSize: 15)
                      ),
                      subtitle: Text('Средние по уровню'),
                      activeColor: Colors.red,
                      onChanged: _toggleTrailsBlueFilter
                  ),
                  CheckboxListTile(
                      value: this.filterTrailsRed,
                      title: Text('Красные',
                          style: TextStyle(color: Colors.red, fontSize: 15)
                      ),
                      subtitle: Text('Для опытных'),
                      activeColor: Colors.red,
                      onChanged: _toggleTrailsRedFilter
                  ),
                  CheckboxListTile(
                      value: this.filterTrailsBlack,
                      title: Text(
                          'Черные',
                          style: TextStyle(fontSize: 15)
                      ),
                      subtitle: Text('Самые сложные'),
                      activeColor: Colors.red,
                      onChanged: _toggleTrailsBlackFilter
                  ),
                  CheckboxListTile(
                      value: this.filterTrailsGreen,
                      title: Text(
                          'Зеленые',
                          style: TextStyle(color: Colors.green, fontSize: 15)
                      ),
                      activeColor: Colors.red,
                      subtitle: Text('Для начинающих'),
                      onChanged: _toggleTrailsGreenFilter
                  ),

                  Divider(color: Colors.grey, height: 1),

                  ListTile(
                      title: Text(
                          'О приложении',
                          style: TextStyle(fontSize: 16)
                      ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AboutPage()),
                      );
                    },
                  ),
                ],
              )
            ),
            backgroundColor: Colors.red,
            appBar: TabBar(
              indicatorColor: Colors.red,
              tabs: [
                Tab(child: Text('Роза Хутор')),
                Tab(child: Text('Газпром')),
                Tab(child: Text('Горки город')),
              ],
              onTap: (tabIndex) {
                if (tabIndex == 0) {
                  this.resortType = ResortType.RosaKhutor;

                } else if (tabIndex == 1) {
                  this.resortType = ResortType.Gazprom;

                } else if (tabIndex == 2) {
                  this.resortType = ResortType.GorkyGorod;
                }
              },
            ),
            body: TabBarView(
              children: [
                ResortTrailsTab(_getTrailsWithFilters(this.rosaKhutorTrails)),
                ResortTrailsTab(_getTrailsWithFilters(this.gazpromTrails)),
                ResortTrailsTab(_getTrailsWithFilters(this.gorkyGorodTrails)),
              ],
            ),
            floatingActionButton: FloatingActionButton(
              tooltip: "Обновить",
              onPressed: () {
                setState(() {
                  if (this.resortType == ResortType.RosaKhutor) {
                    getRosaKhutorTrailsData(context).then((items) => _appendItems(ResortType.RosaKhutor, items));

                  } else if (this.resortType == ResortType.Gazprom) {
                    getGazpromTrailsData(context).then((items) => _appendItems(ResortType.Gazprom, items));

                  } else if (this.resortType == ResortType.GorkyGorod) {
                    getGorkyGorodTrailsData(context).then((items) => _appendItems(ResortType.GorkyGorod, items));
                  }
                });

              },
              child: Icon(Icons.refresh),
              foregroundColor: Colors.white,
              backgroundColor: Colors.red,
            ),
          ),
        ),
      ),
    );
  }

  _toggleTrailsOpenedFilter(bool value) => setState(() => this.filterTrailsOpened = value);

  _toggleTrailsBlackFilter(bool value) => setState(() => this.filterTrailsBlack = value);
  _toggleTrailsRedFilter(bool value) => setState(() => this.filterTrailsRed = value);
  _toggleTrailsBlueFilter(bool value) => setState(() => this.filterTrailsBlue = value);
  _toggleTrailsGreenFilter(bool value) => setState(() => this.filterTrailsGreen = value);
}

class ResortTrailsTab extends StatelessWidget {
  final List<Trail> trails = [];

  ResortTrailsTab(List<Trail> trails) {
    this.trails.clear();
    this.trails.addAll(trails);
  }

  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.all(2.0),
        color: Colors.white,
        child: ListView(
            children: trails.map(_buildItem).toList()
        )
    );
  }

  Widget _buildItem(Trail trail) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: Table(
        defaultVerticalAlignment: TableCellVerticalAlignment.middle,
        columnWidths: {1: FractionColumnWidth(.2)},
        children: [
          TableRow(
            children: [
              ListTile(
                title: Text(trail.name,
                  style: TextStyle(color: Colors.black, fontSize: 16),
                ),
                subtitle: Text(_getDifficultyLabel(trail.difficulty),
                  style: TextStyle(color: Colors.grey, fontSize: 14),
                ),
              ),
              _getTrailStatusWidget(trail.status)
            ]
          ),
        ],
      )
    );
  }

  String _getDifficultyLabel(TrailDifficulty difficulty) {
    String result;

    if (difficulty == TrailDifficulty.Green) {
      result = "Зеленая";

    } else if (difficulty == TrailDifficulty.Blue) {
      result = "Синяя";

    } else if (difficulty == TrailDifficulty.Red) {
      result = "Красная";

    } else if (difficulty == TrailDifficulty.Black) {
      result = "Черная";

    } else {
      result = "Неизвестная";
    }

    return "Сложность: $result";
  }

  Widget _getTrailStatusWidget(TrailStatus status) {
    Color color;

    if (status == TrailStatus.Open) {
      color = Colors.green;

    } else if (status == TrailStatus.Closed) {
      color = Colors.red;

    } else {
      color = Colors.grey;
    }

    return Container(
      width: 15.0,
      height: 15.0,
      padding: const EdgeInsets.all(30.0),
      decoration: BoxDecoration(
          color: color,
          shape: BoxShape.circle
      ),
    );
  }
}