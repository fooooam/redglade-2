enum TrailDifficulty {
  Green, Blue, Red, Black, Unknown
}

enum TrailStatus {
  Open, Closed
}

class Trail {
  final String name;
  final String locationName;
  final TrailDifficulty difficulty;
  final int length;
  final TrailStatus status;

  Trail(this.name, this.locationName, this.difficulty, this.length, this.status);
}