import 'package:flutter/material.dart';
import 'package:html/dom.dart' as dom;
import 'package:html/parser.dart' show parse;
import 'package:http/http.dart' as http;
import 'package:try_flutter/models/trail.dart';

Future<List<Trail>> getRosaKhutorTrailsData(BuildContext context) async {
  print("loading rosa khutor data..");

  final String url = "https://rosaski.com/skiing/trails/";

  var response = await http.get(Uri.encodeFull(url), headers: {});

  var document = parse(response.body);

  final List<dom.Element> elements = document.querySelectorAll(".trails_row");

  final List<Trail> trails = [];

  elements.skip(1).forEach((element) => {


    trails.add(
        Trail(
            element.querySelector(".trails_cell--title").text.trim(),
            "",
            _getTrailDifficulty(element),
            0,
            _getTrailStatus(element)
        )
    )
  });

  return trails;
}

TrailDifficulty _getTrailDifficulty(dom.Element element) {
  TrailDifficulty difficulty;

  var difficultyElement = element.querySelector(".trails_cell--complexity");

  var grayLevelElement = difficultyElement.querySelector(".trails_cell--complexity--grey");

  print(grayLevelElement);

  if (grayLevelElement != null) {
    difficulty = TrailDifficulty.Black;

  } else {
    var blueLevelElement = difficultyElement.querySelector(".trails_cell--complexity--blue");

    if (blueLevelElement != null) {
      difficulty = TrailDifficulty.Blue;

    } else {
      difficulty = TrailDifficulty.Unknown;
    }
  }

  return difficulty;
}

TrailStatus _getTrailStatus(dom.Element element) {
  var statusElement = element.querySelector(".trails_cell--status");

  var statusIconElement = statusElement.querySelector(".lifts_svg--grey");

  TrailStatus trailStatus;

  if (statusIconElement != null) {
    trailStatus = TrailStatus.Closed;

  } else {
    trailStatus = TrailStatus.Open;
  }

  return trailStatus;
}