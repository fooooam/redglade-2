import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:try_flutter/models/trail.dart';

Future<List<Trail>> getGorkyGorodTrailsData(BuildContext context) async {
  print("loading gorkygorod data..");

  final String url = "https://gorkygorod.ru/traildata.json?season=winter";

  final Response response = await http.get(
      Uri.encodeFull(url),
      headers: {
        'X-Requested-With': 'XMLHttpRequest'
      }
  );

  final Map<String, dynamic> data = jsonDecode(response.body);

  final List<Trail> trails = [];

  final List<dynamic> workingTrailsJson = data['working'];

  workingTrailsJson.forEach((trailJson) => {
    trails.add(
        Trail(
            trailJson['name'],
            trailJson['sector'],
            _getTrailDifficultyFromString(trailJson['level']),
            0,
            TrailStatus.Open
        )
    )
  });

  final List<dynamic> inactiveTrailsJson = data['inactive'];

  inactiveTrailsJson.forEach((trailJson) => {
    trails.add(
        Trail(
            trailJson['name'],
            trailJson['sector'],
            _getTrailDifficultyFromString(trailJson['level']),
            0,
            TrailStatus.Closed
        )
    )
  });

  final List<dynamic> pausedTrailsJson = data['paused'];

  pausedTrailsJson.forEach((trailJson) => {
    trails.add(
        Trail(
            trailJson['name'],
            trailJson['sector'],
            _getTrailDifficultyFromString(trailJson['level']),
            0,
            TrailStatus.Closed
        )
    )
  });

  return trails;
}

TrailDifficulty _getTrailDifficultyFromString(String value) {
  TrailDifficulty difficulty;

  if (value == "Легкая:Beginner") {
    difficulty = TrailDifficulty.Green;

  } else if (value == "Средняя:Moderate") {
    difficulty = TrailDifficulty.Blue;

  } else if (value == "Очень высокая:Pro") {
    difficulty = TrailDifficulty.Black;

  } else {
    difficulty = TrailDifficulty.Blue;
  }

  return difficulty;
}

