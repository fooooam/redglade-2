import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:try_flutter/models/trail.dart';

Future<List<Trail>> getGazpromTrailsData(BuildContext context) async {
  print("loading gazprom data..");

  final String url = "https://polyanaski.ru/local/ajax/tracks.php";

  final Response response = await http.get(Uri.encodeFull(url), headers: {});

  final Map<String, dynamic> data = jsonDecode(response.body);

  final List<dynamic> trailsJson = data['Горнолыжные трассы'];

  final List<Trail> trails = [];

  trailsJson.forEach((trailJson) => {
    print("track: ${trailJson['name']}"),
    trails.add(
        Trail(
            trailJson['name'],
            "",
            _getGazpromTrailDifficulty(trailJson['difficulty']),
            0,
            _getGazpromTrailStatus(trailJson['status']))
    )
  });

  return trails;
}

TrailDifficulty _getGazpromTrailDifficulty(String difficulty) {
  TrailDifficulty result;

  if (difficulty == "green") {
    result = TrailDifficulty.Green;

  } else if (difficulty == "blue") {
    result = TrailDifficulty.Blue;

  } else if (difficulty == "red") {
    result = TrailDifficulty.Red;

  } else if (difficulty == "black") {
    result = TrailDifficulty.Black;

  } else {
    result = TrailDifficulty.Unknown;
  }

  return result;
}

TrailStatus _getGazpromTrailStatus(int status) {
  if (status == 1) {
    return TrailStatus.Open;
  } else {
    return TrailStatus.Closed;
  }
}