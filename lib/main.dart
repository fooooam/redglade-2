import 'package:flutter/material.dart';
import 'package:try_flutter/pages/about.dart';
import 'package:try_flutter/pages/home.dart';

void main() => runApp(RedGladeApp());

class RedGladeApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Красная поляна',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.red,
        primaryColor: Colors.white,
        accentColor: Colors.white,
        tabBarTheme: TabBarTheme(
          labelColor: Colors.white, unselectedLabelColor: Colors.white,
          labelStyle: TextStyle(
              fontSize: 17.0, color: Colors.white
          ),
          unselectedLabelStyle: TextStyle(
              fontSize: 17.0, color: Colors.white
          ),

        )
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => HomePage(),
        '/about': (context) => AboutPage()
      },
    );
  }
}
